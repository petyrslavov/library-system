﻿using Microsoft.EntityFrameworkCore;
using System;
using WebAPI.Data.Configurations;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class LibraryDbContext: DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Book> Books { get; set; }

        public DbSet<BookRequest> BookRequests { get; set; }

        public LibraryDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BookConfiguration());

            modelBuilder.Entity<BookRequest>()
                .HasOne(u => u.User)
                .WithMany(br => br.BookRequests)
                .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<BookRequest>()
                .HasOne(b => b.Book)
                .WithMany(br => br.BookRequests)
                .HasForeignKey(b => b.BookId);
        }
    }
}

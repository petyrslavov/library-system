﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Models.BindingModels;
using WebAPI.Services;
using WebAPI.Services.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly LibraryDbContext context;
        private readonly IUserService userService;

        public UserController(
            LibraryDbContext context,
            IUserService userService
            )
        {
            this.context = context;
            this.userService = userService;
        }

        [HttpPost]
        public async Task<ActionResult<User>> AddUser([FromBody] UserBindingModel model)
        {
            var user = await userService.CreateUser(model.FirstName, model.Surname, model.LastName);

            await context.SaveChangesAsync();

            return user;
        }
    }
}
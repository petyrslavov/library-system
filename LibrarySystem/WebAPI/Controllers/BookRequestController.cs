﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Models.BindingModels;
using WebAPI.Services.Extensions;
using WebAPI.Services.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookRequestController : ControllerBase
    {
        private const int  randomUserId = 1;

        private readonly LibraryDbContext context;
        private readonly IBookRequestService bookRequestService;

        public BookRequestController(
            LibraryDbContext context,
            IBookRequestService bookRequestService)
        {
            this.context = context;
            this.bookRequestService = bookRequestService;
        }

        [HttpGet]
        public async Task<IEnumerable<Book>> GetAllBooks()
        {
            return await this.bookRequestService.GetAllBooks();
        }

        [HttpPost("{id:int}")]
        public async Task<ActionResult<BookRequest>> BorrowBook([FromRoute] int id, [FromBody] BookRequestBindingModel model)
        {
            var bookRequest = await this.bookRequestService.BorrowBook(randomUserId, id, model.BorrowDate, model.ReturnDate);

            if (bookRequest == null)
            {
                return NoContent();
            }

            await this.context.SaveChangesAsync();

            return bookRequest;
        }

        [HttpGet("search")]
        public async Task<IEnumerable<BookRequest>> SearchRequest([FromQuery] BookRequestSearchFilter filter)
        {
            var bookRequests = await this.bookRequestService.Search(filter);

            return bookRequests;
        }

        [Route("{status}")]
        [HttpGet]
        public async Task<int> GetRequestsByStatus([FromRoute] string status)
        {
            int bookRequestsCount = await this.bookRequestService.GetRequestsByStatus(status);

            return bookRequestsCount;
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<BookRequest>> ChangeStatus(int id, [FromBody] string status)
        {
            if (HttpContext.User.Identity.Name == "Pesho")
            {
                var bookRequest = await this.bookRequestService.ChangeStatus(id, status);

                await this.context.SaveChangesAsync();

                return bookRequest;
            }
            else
            {
                throw new InvalidOperationException("You dont have permission to change status!");
            }
            
        }
    }
}
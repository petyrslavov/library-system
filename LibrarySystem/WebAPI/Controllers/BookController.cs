﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Models.BindingModels;
using WebAPI.Services.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly LibraryDbContext context;
        private readonly IBookService bookService;

        public BookController(
            LibraryDbContext context,
            IBookService bookService
            )
        {
            this.context = context;
            this.bookService = bookService;
        }

        [HttpPost]
        public async Task<ActionResult<Book>> AddBook([FromBody] BookBindingModel model)
        {
            var book = await bookService.AddBook(model.Title, model.Author, model.Quantity);

            await context.SaveChangesAsync();

            return book;
        }

        [Route("{title}")]
        [HttpGet]
        public async Task<ActionResult<Book>> GetBook([FromRoute] string title)
        {
            var book = await this.bookService.GetBook(title);

            if (book == null)
            {
                return this.NotFound();
            }

            return book;
        }
    }
}
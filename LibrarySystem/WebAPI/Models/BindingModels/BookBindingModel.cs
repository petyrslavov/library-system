﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.BindingModels
{
    public class BookBindingModel
    {
        public string Title { get; set; }

        public string Author { get; set; }

        public int Quantity { get; set; }
    }
}

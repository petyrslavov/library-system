﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.BindingModels
{
    public class BookRequestBindingModel
    {
        public DateTime BorrowDate { get; set; }

        public DateTime ReturnDate { get; set; }
    }
}

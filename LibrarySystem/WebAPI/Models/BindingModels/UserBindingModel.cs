﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.BindingModels
{
    public class UserBindingModel
    {
        [Required]
        public string FirstName { get; set; }

        public string Surname { get; set; }

        [Required]
        public string LastName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Services.Extensions;

namespace WebAPI.Services.Interfaces
{
    public interface IBookRequestService
    {
        Task<IEnumerable<Book>> GetAllBooks();

        Task<BookRequest> BorrowBook(int userId, int bookId, DateTime borrowBook, DateTime returnBook);

        Task<IEnumerable<BookRequest>> Search(BookRequestSearchFilter filter);

        Task<int> GetRequestsByStatus(string status);

        Task<BookRequest> ChangeStatus(int id, string status);
    }
}

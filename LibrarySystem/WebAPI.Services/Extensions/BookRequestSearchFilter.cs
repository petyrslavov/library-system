﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Services.Extensions
{
    public class BookRequestSearchFilter
    {
        public string Title { get; set; }

        public string FirstName { get; set; }

        public Status? Status { get; set; }
    }
}

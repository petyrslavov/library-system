﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Services.Extensions
{
    public static class BookRequestSearchExtensionMethod
    {
        public static IQueryable<BookRequest> Search(this IQueryable<BookRequest> query, BookRequestSearchFilter filter)
        {
            if (!string.IsNullOrWhiteSpace(filter.Title))
            {
                query = query
                    .Where(x => x.Book.Title.Trim().ToLower() == filter.Title.Trim().ToLower());
            }

            if (!string.IsNullOrWhiteSpace(filter.FirstName))
            {
                query = query
                    .Where(x => x.User.FirstName.Trim().ToLower() == filter.FirstName.Trim().ToLower());
            }

            if (filter.Status.HasValue)
            {
                query = query
                    .Where(x => x.Status == filter.Status);
            }

            return query;
        }
    }
}

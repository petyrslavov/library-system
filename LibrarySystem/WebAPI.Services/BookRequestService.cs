﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Services.Extensions;
using WebAPI.Services.Interfaces;

namespace WebAPI.Services
{
    public class BookRequestService : IBookRequestService
    {
        private readonly LibraryDbContext context;

        public BookRequestService(LibraryDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Book>> GetAllBooks()
        {
            var books = await this.context.Books.ToListAsync();

            return books;
        }

        public async Task<BookRequest> BorrowBook(int userId, int bookId, DateTime borrowDate, DateTime returnDate)
        {
            var user = await this.context.Users
                .Include(r => r.BookRequests)
                .SingleOrDefaultAsync(u => u.Id == userId);

            var book = await this.context.Books
                .SingleOrDefaultAsync(b => b.Id == bookId);

            var requestedBook = user.BookRequests
                .Where(b => b.Book.Id == book.Id)
                .Where(s => s.Status == Status.Requested || s.Status == Status.PendingReturn)
                .FirstOrDefault();

            BookRequest bookRequest = new BookRequest();

            if (requestedBook != null)
            {
                bookRequest.RequestDate = DateTime.UtcNow;
                bookRequest.Status = Status.Rejected;

                await this.context.BookRequests.AddAsync(bookRequest);
                await this.context.SaveChangesAsync();

                throw new InvalidOperationException("This user already requested this book or book is waiting to be returned");
            }

            int requestedBooksCount = await this.context.BookRequests
                .Where(b => b.Book.Id == book.Id)
                .Where(s => s.Status == Status.Requested).CountAsync();

            int pendingReturnBooksCount = await this.context.BookRequests
               .Where(b => b.Book.Id == book.Id)
               .Where(s => s.Status == Status.PendingReturn).CountAsync();

            int availableBookCount = requestedBooksCount + pendingReturnBooksCount;

            if (availableBookCount > book.Quantity)
            {
                bookRequest.RequestDate = DateTime.UtcNow;
                bookRequest.Status = Status.Rejected;

                await this.context.BookRequests.AddAsync(bookRequest);
                await this.context.SaveChangesAsync();

                throw new InvalidOperationException("Not enough available books");
            }

            bookRequest.RequestDate = DateTime.UtcNow;
            bookRequest.BorrowDate = borrowDate;
            bookRequest.ReturnDate = returnDate;
            bookRequest.Status = Status.Requested;
            bookRequest.BookId = book.Id;
            bookRequest.Book = book;
            bookRequest.UserId = user.Id;
            bookRequest.User = user;

            user.BookRequests.Add(bookRequest);
            this.context.BookRequests.Add(bookRequest);

            return bookRequest;
        }

        public async Task<IEnumerable<BookRequest>> Search(BookRequestSearchFilter filter)
        {
            var bookRequests2 = await context.BookRequests
                .Include(e => e.Book)
                .Include(e => e.User)
                .Search(filter)
                .OrderBy(x => x.Id)
                .ToListAsync();

            return bookRequests2;
        }

        public async Task<int> GetRequestsByStatus(string status)
        {
            var bookRequests = await this.context.BookRequests.ToListAsync();

            if (status == "Requested")
            {
                return bookRequests.Where(s => s.Status.ToString() == "Requested").Count();
            }
            else if (status == "Rejected")
            {
                return bookRequests.Where(s => s.Status.ToString() == "Rejected").Count();
            }
            else if (status == "Returned")
            {
                return bookRequests.Where(s => s.Status.ToString() == "Returned").Count();
            }
            else if (status == "PendingReturn")
            {
                return bookRequests.Where(s => s.Status.ToString() == "PendingReturn").Count();
            }
            else
            {
                throw new ArgumentException("Invalid Status");
            }
        }

        public async Task<BookRequest> ChangeStatus(int id, string newStatus)
        {
            var bookRequest = await this.context.BookRequests.SingleOrDefaultAsync(x => x.Id == id);

            if (bookRequest != null)
            {
                Status status = (Status)Enum.Parse(typeof(Status), newStatus);

                bookRequest.Status = status;
            }

            return bookRequest;
        }
    }
}

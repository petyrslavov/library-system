﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Services.Interfaces;

namespace WebAPI.Services
{
    public class UserService : IUserService
    {
        private readonly LibraryDbContext context;
        private static Random random = new Random();

        public UserService(LibraryDbContext context)
        {
            this.context = context;
        }

        public async Task<User> CreateUser(string firstName, string surname, string lastName)
        {
            User user = new User()
            {
                FirstName = firstName,
                Surname = surname,
                LastName = lastName,
                MembershipNumber = MembershipNumberGenerator()
            };

            await this.context.Users.AddAsync(user);

            return user;
        }

        public string MembershipNumberGenerator()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var membershipNumber =  new string(Enumerable.Repeat(chars, 10)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            var numbersFromDb= this.context.Users.Select(x => x.MembershipNumber).ToList();

            foreach (var number in numbersFromDb)
            {
                if (membershipNumber == number)
                {
                    MembershipNumberGenerator();
                }
            }

            return membershipNumber;
        }
    }
}

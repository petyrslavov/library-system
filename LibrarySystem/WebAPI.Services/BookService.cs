﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Services.Interfaces;

namespace WebAPI.Services
{
    public class BookService : IBookService
    {
        private readonly LibraryDbContext context;

        public BookService(LibraryDbContext context)
        {
            this.context = context;
        }

        public async Task<Book> AddBook(string title, string author, int quantity)
        {
            Book book = new Book()
            {
                Title = title,
                Author = author,
                Quantity = quantity,
            };

            await this.context.Books.AddAsync(book);

            return book;
        }

        public async Task<Book> GetBook(string title)
        {
            var book = await this.context.Books.SingleOrDefaultAsync(b => b.Title == title);

            return book;
        }
    }
}

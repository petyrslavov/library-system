﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebAPI.Models
{
    [Table("book_request")]
    public class BookRequest
    {
        public int Id { get; set; }

        public DateTime RequestDate { get; set; } = DateTime.UtcNow;

        public DateTime BorrowDate { get; set; }

        public DateTime ReturnDate { get; set; }

        public Status Status { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebAPI.Models
{
    public enum Status
    {
        Requested = 1,
        PendingReturn = 2,
        Returned = 3,
        Rejected = 4
    }
}

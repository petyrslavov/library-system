﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebAPI.Models
{
    public class Book
    {
        public Book()
        {
            this.BookRequests = new List<BookRequest>();
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public int Quantity { get; set; }

        public ICollection<BookRequest> BookRequests { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebAPI.Models
{
    [Table("user")]
    public class User
    {
        public User()
        {
            this.BookRequests = new List<BookRequest>();
        }

        public int Id { get; set; }

        public string MembershipNumber { get; set; }

        [Required]
        public string FirstName { get; set; }

        public string Surname { get; set; }

        [Required]
        public string LastName { get; set; }

        public ICollection<BookRequest> BookRequests { get; set; }
    }
}
